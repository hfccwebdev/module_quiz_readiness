(function ($) {

  $(document).ready(function() {
    $('#quiz-readiness-form').submit(function() {

      var total = 0;
      var x = 0;

      for(x =1; x <= 15 ; x++) {
        var questionnumber = 'question' + x;
        answer = parseInt($("input[name=" + questionnumber  + "]:checked").val());
        score = isNaN(answer) ? 0 : answer;
        total += score;
      }

      var msg = "<h3>You scored " + total + " out of 45.</h3>";

      if(total <= 25 ) {
        msg += '<p>You may want to consider taking an on-campus class.</p>';
      }
      else if(total <= 35) {
        msg += '<p>Consider taking an online class, but you’ll likely have to put in extra time and effort.</p>';
      }
      else if(total <= 45) {
        msg += '<p>An online class may be perfect for you.</p>';
      }

      $('#quiz_readiness-info').html(msg);

      return false;

    });
  });
})(jQuery);
